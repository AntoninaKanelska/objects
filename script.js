/*

1.Функція, яка є властивістю об'єкта, називається його методом.
2.Числа, рядки, булеві значення, масиви, об'єкти, функції
3.Об'єкт це посилальний тип даних оскільки зберігає посилання на пам'ять, де знаходиться сам об'єкт
*/

const product = {
    name: "Gadget",
    price: 100,
    discount: 0.1,
    getFinalPrice: function() {
        const finalPrice = this.price * (1 - this.discount);
        return finalPrice;
    }
};

console.log(`Final price after discount is: ${product.getFinalPrice()}`);

function greeting(user) {
    return `Привіт, мені ${user.age} років`;
}


const userName = prompt("Введіть своє ім'я:");
const userAge = prompt("Введіть свій вік:");

const user = {
    name: userName,
    age: userAge
};

const greetingMessage = greeting(user);

alert(greetingMessage);




function deepClone(obj) {
    if (obj === null || typeof obj !== "object") {
        return obj;
    }


    if (Array.isArray(obj)) {
        const arrCopy = [];
        for (let i = 0; i < obj.length; i++) {
            arrCopy[i] = deepClone(obj[i]);
        }
        return arrCopy;
    }


    const objCopy = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            objCopy[key] = deepClone(obj[key]);
        }
    }

    return objCopy;
}


const original = {
    name: "Gadget",
    details: {
        price: 100,
        discount: 0.1
    },
    tags: ["tech", "gadget"]
};

const cloned = deepClone(original);

console.log('Original:', original);
console.log('Cloned:', cloned);
